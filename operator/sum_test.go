package operator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSumnumberSuccess(t *testing.T) {
	expected := 7
	actual := sumTwoNumbers(3, 4)
	assert.Equal(t, actual, expected)
}

func TestSumnumberFail(t *testing.T) {
	expected := 123
	actual := sumTwoNumbers(3, 4)
	assert.NotEqual(t, actual, expected)
}
